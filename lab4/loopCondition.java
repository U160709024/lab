import java.io.IOException;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);

		System.out.print("Player 1 enter row number:");
		int row1 = reader.nextInt();
		System.out.print("Player 1 enter column number:");
		int col1 = reader.nextInt();
		board[row1 - 1][col1 - 1] = 'X';
		printBoard(board);

		System.out.print("Player 2 enter row number:");
		int row2 = reader.nextInt();
		System.out.print("Player 2 enter column number:");
		int col2 = reader.nextInt();
                
				do {                
                    if(row1 == row2 && col1 == col2){
                       
                        System.out.println("This is not an empty box! Please select different numbers. ");  
                    
                        break;
                    }    
                    else{    
                        board[row2 - 1][col2 - 1] = 'O';
                    
                        printBoard(board);

                    reader.close();
                    }
                    
                    
                } while (true);
 	
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
