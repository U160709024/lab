

// CHECKBOARD!

// We are checking is there any empty cell exist or not.
// If it is true, there is an empty cell. Otherwise board is full.
public boolean isFull(){
            
            boolean isFull = true;
            
            for(int row=0 ; row<3 ; row++){
                for(int col=0; col<3 ; col++){
                    if(board[row][col] == '-')
                        isFull = true;
					System.out.println(Game ended with a draw.);
                }
            }
            
			return isFull();
			
            
}

// Returns true if there is a win, false otherwise.
public boolean checkForWin(){

	return (checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin());
}


// Let's check the rows and see if any are winners.
public boolean checkRowsForWin() {
	for (int row = 0; row < 3; i++) {
        if (checkRowCol(board[row][0], board[row][1], board[row][2]) == true) {           
			return true;
        }
	}
	return false;
}


// Let's check columns and see if any are winners.
public boolean checkColumnsForWin() {

    for (int col = 0; col < 3; i++) {
        if (checkRowCol(board[0][col], board[1][col], board[2][col]) == true) {
            return true;
        }
    }
    return false;
}

// Check the two diagonals to see if either is a win. Return true if either wins.
public boolean checkDiagonalsForWin() {
    return ((checkRowCol(board[0][0], board[1][1], board[2][2]) == true) || (checkRowCol(board[0][2], board[1][1], board[2][0]) == true));
}















