
public class FindPrimes{
	public static void main(String[] args){
		int sayi=Integer.parseInt(args[0]);

		for(int x=sayi;x>0;x--){
			int result=0;
			for(int i=1;i<=x;i++){
				if(x%i==0){
					result++;
				}
		
			}
			if(result==2){
				System.out.println(x);
			}
		}
	}	
}
=======

public class FindPrimes {

	public static void main(String[] args) {
		int limit = Integer.parseInt(args[0]);
		
		for (int number = 2; number <limit; number++) {

			if (isPrimeNumber(number)){
				System.out.print(number +" ");
			}
		}

	}
	
	public static boolean isPrimeNumber(int number) {
		boolean isPrime = true;
		
		//check prime
		for (int i =2; i<number; i++) {
			if(number % i == 0) {
				isPrime = false;
				break;
			}
		}
		return isPrime;		
	}

}

