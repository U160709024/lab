/*
 Java Program to Implement Euclid GCD Algorithm
 */

import java.util.Scanner;
 
public class GCDLoop    
{
  
    public long gcd(long p, long q)
    {
        if (p % q == 0)
            return q;
        return gcd(q, p % q);
    }
    /** Main function **/
    public static void main(String[] args){ 
    
        Scanner scan = new Scanner(System.in);
        /** Make an object of EuclidGcd class **/
        GCDLoop eg = new GCDLoop();
 
        /** Accept two integers **/
        System.out.print("Enter two integer numbers: ");
        long n1 = scan.nextLong();
        long n2 = scan.nextLong();
   
        long gcd = eg.gcd(n1, n2);
        System.out.println("\nGCD of "+ n1 +" and "+ n2 +" = "+ gcd);
    }
}